package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        int k = 0;
        int z = 0;
        for (Object o : x) {
            for (int j = k; j < y.size(); j++) {
                if (o.equals(y.get(j))) {
                    z++;
                    k = j;
                    break;
                }
            }
        }
        return z == x.size();
    }
}
