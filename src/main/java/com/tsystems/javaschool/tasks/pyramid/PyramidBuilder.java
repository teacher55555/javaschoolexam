package com.tsystems.javaschool.tasks.pyramid;


import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        List<List<Integer>> matrix = new ArrayList<>();
        try {
            Collections.sort(inputNumbers);
            for (int i = 0, j = 1; i < inputNumbers.size(); ) {
                List<Integer> row = new ArrayList<>();
                for (int k = 0; k < j; k++) {
                    row.add(inputNumbers.get(i++));
                }
                matrix.add(row);
                j++;
            }
        } catch (NullPointerException ex) {
            throw new CannotBuildPyramidException();
        } catch (IndexOutOfBoundsException ex) {
            throw new CannotBuildPyramidException();
        } catch (OutOfMemoryError ex) {
            throw new CannotBuildPyramidException();
        }

        int[][] array = new int[matrix.size()][matrix.size() - 1 + matrix.size()];
        int x = array.length - 1;

        for (int i = 0; i < array.length; i++) {
            List<Integer> list = matrix.get(i);
            int k = 0;
            while (k < x) {
                k++;
            }
            for (Integer integer : list) {
                array[i][k] = integer;
                k += 2;
            }
            x--;
        }
        return array;
    }
}

