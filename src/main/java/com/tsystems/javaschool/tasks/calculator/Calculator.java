package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Calculator {

    private ArrayList<String> opnStr = new ArrayList<>();
    private StringBuilder digitStr = new StringBuilder();
    private ArrayList<Character> stack = new ArrayList<>();
    private ArrayList<String> calculationStack = new ArrayList<>();
    private double d1;
    private double d2;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        // TODO: Implement the logic here


        if (statement != null && !statement.contains("..") && !statement.contains(",") && !statement.isEmpty()) {
            infixToOpn2(statement);
            calculation();
            String s = calculationStack.get(0);
            if (!s.equals("null")) {
                double d = new BigDecimal(s).setScale(4, RoundingMode.HALF_UP).doubleValue();
                s = d + "";
                if (s.matches("^[-]?[0-9]+[.][0]+$")) {
                    s = (int) d + "";
                }
                return s;
            }
        }
        return null;
    }

    /////////////////////////////Reverse Polish notation (RPN) algorithm ////////////////////////////////////////

    //converts Infix notation to Postfix notation
    private void infixToOpn2(String str) {

        if (str.charAt(0) == '-') {
            str = '0' + str;
        }

        try {
            for (Character c : str.toCharArray()) {
                if (Character.isDigit(c) || c == '.') {
                    digitStr.append(c);
                } else if (c == '/' || c == '*' || c == '+' || c == '-') {
                    digitsToOpnStr();
                    if (stack.size() > 0) {
                        while (stack.get(stack.size() - 1) == '/' || stack.get(stack.size() - 1) == '*'
                                || stack.get(stack.size() - 1) == '+' || stack.get(stack.size() - 1) == '-' ) {
                            if (compare(c, stack.get(stack.size() - 1)) < 1) {
                                opnStr.add(stack.get(stack.size() - 1).toString());
                                stack.remove(stack.size() - 1);
                                if (stack.size() == 0)
                                    break;
                            } else break;
                        }
                    }
                    stack.add(c);
                } else if (c == '(') {
                    digitsToOpnStr();
                    stack.add(c);
                } else if (c == ')') {
                    digitsToOpnStr();
                    while (stack.get(stack.size() - 1) != '(') {
                        opnStr.add(stack.get(stack.size() - 1).toString());
                        stack.remove(stack.size() - 1);
                    }
                    stack.remove(stack.size() - 1);
                }
            }
            digitsToOpnStr();
            if (stack.size() > 0) {
                for (int i = stack.size() - 1; i >= 0; i--) {
                    if (stack.get(i) == '(' || stack.get(i) == ')') {
                        throw new ArrayIndexOutOfBoundsException();
                    }
                    opnStr.add(stack.get(i).toString());
                }
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("brackets are not consistent");
            calculationStack.clear();
            calculationStack.add("null");
        }
    }

    //collects digits. Example (1,1,1 to 111)
    private void digitsToOpnStr() {
        if (digitStr.length() > 0) {
            opnStr.add(digitStr.toString());
            digitStr.delete(0, digitStr.length());
        }
    }

    // priority comparison of the math operations
    private int compare(char a, char b) {
        if (a == '*' || a == '/') {
            if (b == '*' || b == '/') {
                return 0;
            }
            if (b == '-' || b == '+') {
                return 1;
            } else return -1;
        }

        if (a == '+' || a == '-') {
            if (b == '-' || b == '+') {
                return 0;
            } else return -1;
        }
        return 0;
    }

    //Gets each element of postfix notation, does calculates and adds to end of the list "calculationStack" ;
    private void calculation() {
        for (int i = 0; i < opnStr.size(); i++) {
            String s = opnStr.get(i);

            switch (s) {
                case "/":
                    calcD1D2StackInitializator();

                    if (d2 == 0) {
                        calculationStack.clear();
                        calculationStack.add("null");
                        return;
                    }
                    calculationStack.add((d1 / d2) + "");
                    break;

                case "*":
                    calcD1D2StackInitializator();
                    calculationStack.add((d1 * d2) + "");
                    break;

                case "+":
                    calcD1D2StackInitializator();
                    calculationStack.add(d1 + d2 + "");
                    break;

                case "-":
                    calcD1D2StackInitializator();
                    calculationStack.add((d1 - d2) + "");
                    break;

                default:
                    calculationStack.add(s);
                    break;
            }

        }

    }

    // Helper for calculate method: takes last and previous elements of calculationStack,
    // parses string values to double variables and removes them from calculationStack
    private void calcD1D2StackInitializator() {
        try {
            d1 = Double.parseDouble(calculationStack.get(calculationStack.size() - 2));
            d2 = Double.parseDouble(calculationStack.get(calculationStack.size() - 1));
        } catch (Exception ex) {
            calculationStack.clear();
            calculationStack.add("null");
            return;
        }
        calculationStack.remove(calculationStack.size() - 1);
        calculationStack.remove(calculationStack.size() - 1);
    }
}
